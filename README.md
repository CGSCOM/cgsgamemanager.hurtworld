[license]: https://tldrlegal.com/l/mit
[issues]: https://bitbucket.org/CGSCOM/cgsgamemanager.hurtworld/issues

# CGSGameManager [![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat)][License]  [![Build status](https://ci.appveyor.com/api/projects/status/psefoiv59g3xhrq7/branch/master?svg=true)](https://ci.appveyor.com/project/crackedgameservers/cgsgamemanager-hurtworld/branch/master)

## Open Source

CGSGameManager is free, open source software distributed under the [MIT License][license]. We accept and encourage contributions from our community.