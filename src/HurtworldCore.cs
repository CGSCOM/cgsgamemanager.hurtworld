﻿using CGSGameManager.Hurtworld.Plugins;
using Steamworks;
using uLink;

namespace CGSGameManager.Hurtworld
{
    public sealed class HurtworldCore : HurtworldPlugin
    {
        public override string Name => "HurtworldCore";
        private bool Initialized = false;

        public HurtworldCore() : base()
        {
        }

        private void OnPlayerConnected(string name, NetworkPlayer player)
        {
            if (!Initialized)
            {
                GameManager.KickPlayer(player, "Server is loading");
                return;
            }
            LogInfo("OnPlayerConnected Called");

            var session = global::GameManager.Instance.GetSession(player);

            if (session == null)
            {
                LogError("Failed to get player session from Network Player");
                return;
            }

            session.Name = name;
            if (session.Identity != null) session.Identity.Name = name;
            session.ValidationResponse = EAuthSessionResponse.k_EAuthSessionResponseOK;
            session.EACResponse = EasyAntiCheat.Server.UserStatus.UserAuthenticated;
            GameManager.OnPlayerConnected(session);
        }

        private void OnPlayerDisconnected(PlayerSession session)
        {
            if (session == null)
            {
                LogError("Player Session is null");
                return;
            }

            LogInfo("OnPlayerDisconnected Called");

            if (!Initialized)
            {
                GameManager.KickPlayer(session, "Server is loading");
                return;
            }

            GameManager.OnPlayerDisconnected(session);
        }

        private void OnServerSaved()
        {
            if (DEBUG) LogInfo("OnServerSaved Called");
        }

        private void Init()
        {
            if (DEBUG) LogInfo("Init Called");
        }

        private void Loaded()
        {
            if (DEBUG) LogInfo("Loaded Called");
        }

        private object OnEacStatus(PlayerSession session, EasyAntiCheat.Server.UserStatusUpdate status)
        {
            Interface.Core.LogInfo($"[EAC] OnEacStatus Called");

            Core.Interfaces.AntiCheatResponse stats = 0;

            switch (status.Status)
            {
                case EasyAntiCheat.Server.UserStatus.UserAuthenticated:
                    stats = CGSGameManager.Core.Interfaces.AntiCheatResponse.ClientAuthenticatedRemote;
                    break;

                case EasyAntiCheat.Server.UserStatus.UserBanned:
                    stats = CGSGameManager.Core.Interfaces.AntiCheatResponse.ClientBanned;
                    break;

                case EasyAntiCheat.Server.UserStatus.UserDisconnected:
                    stats = CGSGameManager.Core.Interfaces.AntiCheatResponse.ClientDisconnected;
                    break;

                case EasyAntiCheat.Server.UserStatus.UserViolation:
                    stats = CGSGameManager.Core.Interfaces.AntiCheatResponse.ClientViolation;
                    break;
            }

            var update = new Core.Interfaces.AnticheatResponse(session, session.CGSPlayer, status.RequiresKick, status.Message, stats);
            GameManager.AntiCheatManager.HandleAntiCheatResponse(update);

            return true;
        }

        private object OnSteamStatus(CSteamID steamid, CSteamID ownerid, EAuthSessionResponse response)
        {
            GameManager.SteamManager.HandleAuthTicketResponse(steamid.m_SteamID, ownerid.m_SteamID, (Core.Interfaces.SteamAuthTicketResponse)((int)response));
            return true;
        }

        private void OnServerInitialized()
        {
            if (Initialized) return;
            Initialized = true;

            GameManager.SteamManager.Init();
            GameManager.AntiCheatManager.Init();
        }
    }
}
