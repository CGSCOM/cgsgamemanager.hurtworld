﻿using CGSGameManager.Core.Plugins;

namespace CGSGameManager.Hurtworld.Plugins
{
    public abstract class HurtworldPlugin : Plugin
    {
        protected readonly GameServers.HurtworldGameManager GameManager;

        protected HurtworldPlugin()
        {
            GameManager = HurtworldLoader.GameManager;
        }
    }
}
