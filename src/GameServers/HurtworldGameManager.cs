﻿using System.Text;
using CGSGameManager.Core.GameServers;
using uLink;

namespace CGSGameManager.Hurtworld.GameServers
{
    public sealed class HurtworldGameManager : GameManager<HurtworldPlayer, SteamManager, EasyAntiCheatManager>
    {
        #region Player Kicking

        public void KickPlayer(NetworkPlayer player, string reason)
        {
            GameManager.Instance.StartCoroutine(GameManager.Instance.DisconnectPlayerSync(player, (string.IsNullOrEmpty(reason)) ? "Kicked" : reason));
        }

        public void KickPlayer(PlayerSession session, string reason)
        {
            if (session == null) return;
            if (session.Player.isConnected) KickPlayer(session.Player, reason);
        }

        public override void KickPlayer(HurtworldPlayer player, string reason) => KickPlayer(player.Session, reason);

        public override void KickPlayer(ulong steamID, string reason)
        {
            var net = GameManager.Instance.GetNetworkPlayerBySteamId(new Steamworks.CSteamID(steamID));
            if (net.HasValue) KickPlayer(net.Value, reason);
        }

        #endregion

        #region Player Banning

        public override void BanPlayer(ulong steamID, string reason)
        {
            var csteam = new Steamworks.CSteamID(steamID);
            BanManager.Instance.AddBan(steamID);
        }

        public override void BanPlayer(HurtworldPlayer player, string reason) => BanPlayer(player.SteamID, reason);

        public void BanPlayer(PlayerSession session, string reason) => BanPlayer(session.SteamId.m_SteamID, reason);

        public void BanPlayer(PlayerIdentity identity, string reason) => BanPlayer(identity.SteamId.m_SteamID, reason);

        public void BanPlayer(NetworkPlayer player, string reason)
        {
            var session = GameManager.Instance.GetSession(player);
            if (session != null) BanPlayer(session, reason);
        }

        #endregion

        #region Chat

        public override void SendChatMessage(HurtworldPlayer player, string message, string prefix = null)
        {
            var sb = new StringBuilder();
            if (string.IsNullOrEmpty(prefix)) sb.Append("[SERVER]: "); else sb.AppendFormat("{0}: ", prefix);
            sb.Append(message);
            if (player == null) ChatManagerServer.Instance.AppendChatboxServerAll(sb.ToString());
            else ChatManagerServer.Instance.AppendChatboxServerSingle(sb.ToString(), player.Session.Player, UnityEngine.Color.white);
        }

        public override void SendAdminMessage(string message, string prefix = null)
        {
            foreach (var player in GameManager.Instance.GetSessions())
            {
                if (!GameManager.Instance.IsAdmin(player.Value.SteamId)) continue;

                SendChatMessage(player.Value.CGSPlayer as HurtworldPlayer, message, prefix);
            }
        }

        #endregion

        public void OnPlayerDisconnected(PlayerSession session) => OnPlayerDisconnected(session.CGSPlayer as HurtworldPlayer);

        public void OnPlayerConnected(PlayerSession session)
        {
            if (session == null || (session?.Player.isConnected ?? false)) return;
            var cgs = new HurtworldPlayer(session, GetUserData(session.SteamId.m_SteamID));
            session.CGSPlayer = cgs;

            if (!SteamManager.StartSteamSession(session.AuthTicketBuffer, session.SteamId.m_SteamID))
            {
                cgs.UserData.OwnerID = SteamManager.CreateBotID();
                session.CGSPlayer.UserData.IsSpacewarsUser = true;
                SteamManager.UpdateUser(session.CGSPlayer.OwnerID, session.CGSPlayer.DisplayName, 1);
            }
            else
            {
                session.CGSPlayer.UserData.IsSpacewarsUser = false;
                SteamManager.UpdateUser(session.CGSPlayer.SteamID, session.CGSPlayer.DisplayName, 1);
            }

            AntiCheatManager.EnrollUser(session.CGSPlayer as HurtworldPlayer);
            Interface.Core.LogInfo("[HurtworldUserManager] Registered User with eac");
            session.EACResponse = EasyAntiCheat.Server.UserStatus.UserAuthenticated;
            if (session.CGSPlayer.IsConnected)
            {
                OnPlayerConnected(session.CGSPlayer as HurtworldPlayer);
            }
        }

        protected override void OnPlayerConnected(HurtworldPlayer player)
        {
            player.UserData.OnConnected(player);

            Interface.Core.LogInfo($"[Users] {player.DisplayName}/{player.SteamID} connected with {((player.IsSpacewars) ? "Spacewars" : "Steam")}");
        }

        protected override void OnPlayerDisconnected(HurtworldPlayer player)
        {
            if (player == null) return;

            AntiCheatManager.UnenrollUser(player);
            if (player.IsSpacewars) SteamManager.EndSession(player.OwnerID);
            else SteamManager.EndSession(player.SteamID);

            player.UserData.OnDisconnected(player);

            Interface.Core.LogInfo($"[Users] {player.DisplayName}/{player.SteamID} has disconnected");
        }
    }
}
