﻿using CGSGameManager.Core.GameServers;

namespace CGSGameManager.Hurtworld.GameServers
{
    public sealed class HurtworldPlayer : Player
    {
        public PlayerSession Session { get; }

        public HurtworldPlayer(PlayerSession session, UserData data) : base(data)
        {
            Session = session;
        }

        public override ulong SteamID => Session?.SteamId.m_SteamID ?? UserData.SteamID;

        public override string IPAddress => Session?.Player.ipAddress ?? UserData.IPAddress;

        public override string DisplayName
        {
            get => Session?.Identity?.Name ?? UserData.DisplayName; protected set
            {
                if (Session != null)
                {
                    if (Session.Identity != null) Session.Identity.Name = ChatManagerServer.CleanupGeneral(value);
                }
                UserData.DisplayName = ChatManagerServer.CleanupGeneral(value);
            }
        }

        public override bool IsConnected => Session?.Player.isConnected ?? false;
    }
}
