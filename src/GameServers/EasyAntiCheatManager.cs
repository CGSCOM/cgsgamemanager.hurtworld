﻿using System;
using CGSGameManager.Core.Interfaces;
using EasyAntiCheat.Server;
using UnityEngine;

namespace CGSGameManager.Hurtworld.GameServers
{
    public sealed class EasyAntiCheatManager : IAntiCheatProvider<HurtworldPlayer>
    {
        public class EACScript : MonoBehaviour
        {
            GameManager instance;
            EasyAntiCheatManager manager;

            UserStatusHandler handler;


            private void Awake()
            {
                instance = GetComponent<GameManager>();
                manager = HurtworldLoader.GameManager.AntiCheatManager;
                handler = new UserStatusHandler(HandleClientStatus);
                enabled = true;
                Debug.Log("EAC Handler Enabled");
            }

            private void Update()
            {
                if (instance.EACServer == null) return;

                instance.EACServer.HandleUserUpdates(handler);
            }

            public void HandleClientStatus(UserStatusUpdate status)
            {
                PlayerSession session = null;
                uLink.NetworkPlayer? player = instance.GetNetworkPlayerBySteamId(new Steamworks.CSteamID((ulong.Parse(status.User.PlayerGuid))));
                if (player.HasValue)
                {
                    session = instance.GetSession(player.Value);
                    AntiCheatResponse response = 0;
                    switch(status.Status)
                    {
                        case UserStatus.UserAuthenticated:
                            session.EACResponse = UserStatus.UserAuthenticated;
                            response = AntiCheatResponse.ClientAuthenticatedRemote;
                            break;

                        case UserStatus.UserBanned:
                            session.EACResponse = UserStatus.UserBanned;
                            response = AntiCheatResponse.ClientBanned;
                            break;

                        case UserStatus.UserDisconnected:
                            session.EACResponse = UserStatus.UserDisconnected;
                            response = AntiCheatResponse.ClientDisconnected;
                            break;

                        case UserStatus.UserViolation:
                            session.EACResponse = UserStatus.UserViolation;
                            response = AntiCheatResponse.ClientViolation;
                            break;
                    }
                    Debug.Log("Handling EAC Response with CGS");
                    manager.HandleAntiCheatResponse(new AnticheatResponse(session, session.CGSPlayer, status.RequiresKick, status.Message, response));
                    return;
                }

                Debug.Log("Failed to find a compatible client for eac");
            }
        }

        public void Init()
        {
            GameManager.Instance.EACServer.Dispose();
            GameManager.Instance.EACServer = new EasyAntiCheatServer("Hurtworld.Server");
            GameManager.Instance.gameObject.AddComponent<EACScript>();
        }

        public bool EnrollUser(HurtworldPlayer player)
        {
            try
            {
                Interface.Core.LogInfo($"{player.SteamID}, {player.IPAddress}, {player.OwnerID}, {player.DisplayName}");
                GameManager.Instance.EACServer.RegisterUser(player.Session.AuthTicketBuffer, player.SteamID.ToString(), player.OwnerID.ToString(), player.DisplayName, GameManager.Instance.IsAdmin(player.Session.SteamId));
                return true;
            }
            catch (Exception ex)
            {
                Interface.Core.LogException($"[EACManager] Failed to Register User {player.SteamID}/{player.DisplayName} with EAC", ex);
                return false;
            }
        }

        public bool UnenrollUser(HurtworldPlayer player)
        {
            try
            {
                GameManager.Instance.EACServer.UnregisterUser(player.Session.AuthTicketBuffer);
                return false;
            }
            catch (Exception ex)
            {
                Interface.Core.LogException($"[EACManager] Failed to unregister user {player.SteamID}/{player.DisplayName} with EAC", ex);
                return false;
            }
        }


        public void HandleAntiCheatResponse(AnticheatResponse response)
        {
            Interface.Core.LogInfo($"EAC Called");
            Interface.Core.LogInfo($"EACResponse: {response.CGSPlayer.SteamID}/{response.CGSPlayer.DisplayName} Message: {response.Message} | RequiresKick: {response.RequiresKick} | Status: {response.Status}");
            var player = response.CGSPlayer as HurtworldPlayer;

            player.AntiCheatResponse = response.Status;


            switch (response.Status)
            {
                case AntiCheatResponse.ClientBanned:
                    player.Session.EACResponse = UserStatus.UserBanned;
                    HurtworldLoader.GameManager.SendChatMessage(null, $"<color=yellow>{player.DisplayName} was kicked (banned by anticheat.)</color>", "[<color=#00abeb>CGS</color>]");
                    BanManager.Instance.AddBan(player.Session.SteamId.m_SteamID);
                    break;

                case AntiCheatResponse.ClientViolation:
                    player.Session.EACResponse = UserStatus.UserViolation;
                    HurtworldLoader.GameManager.SendAdminMessage($"<color=yellow>{player.DisplayName}/{player.SteamID} has triggered a EAC Violation</color>", "[<color=#00abeb>CGS</color>]");
                    break;

                case AntiCheatResponse.ClientDisconnected:
                    player.Session.EACResponse = UserStatus.UserDisconnected;
                    break;

                case AntiCheatResponse.ClientAuthenticatedRemote:
                    player.Session.EACResponse = UserStatus.UserAuthenticated;
                    break;

                default:
                    break;
            }

            if (response.RequiresKick)
            {
                UnenrollUser(player);
                HurtworldLoader.GameManager.KickPlayer(player, $"EAC: {response.Status}");
            }
        }
    }
}
