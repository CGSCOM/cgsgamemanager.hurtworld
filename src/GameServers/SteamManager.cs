﻿using System;
using CGSGameManager.Core.Interfaces;
using Steamworks;

namespace CGSGameManager.Hurtworld.GameServers
{
    public sealed class SteamManager : ISteamProvider
    {
        public ulong CreateBotID() => SteamGameServer.CreateUnauthenticatedUserConnection().m_SteamID;

        public void Init()
        {
            try
            {
                SteamworksManagerClient.Instance._validateAuthResponseCallback = Callback<ValidateAuthTicketResponse_t>.CreateGameServer(new Callback<ValidateAuthTicketResponse_t>.DispatchDelegate((s) =>
                {
                    Interface.Core.LogInfo($"[SteamManager] Callback Called");
                    HandleAuthTicketResponse(s.m_SteamID.m_SteamID, s.m_OwnerSteamID.m_SteamID, (SteamAuthTicketResponse)((int)s.m_eAuthSessionResponse));
                }));
            }
            catch (Exception ex)
            {
                Interface.Core.LogException("[SteamManager] Failed to Hook Steam Callback", ex);
            }
        }

        public bool StartSteamSession(byte[] token, ulong steamid)
        {
            if (token == null || token?.Length == 0 || steamid == default(ulong)) return false;

            var response = SteamGameServer.BeginAuthSession(token, token.Length, new CSteamID(steamid));

            switch (response)
            {
                case EBeginAuthSessionResult.k_EBeginAuthSessionResultOK:
                case EBeginAuthSessionResult.k_EBeginAuthSessionResultGameMismatch:
                    return true;

                case EBeginAuthSessionResult.k_EBeginAuthSessionResultDuplicateRequest:
                default:
                    HurtworldLoader.GameManager.KickPlayer(steamid, $"Steam: {response}");
                    return false;
            }
        }

        public void EndSession(ulong steamid)
        {
            if (steamid == default(ulong)) return;

            SteamGameServer.EndAuthSession(new CSteamID(steamid));
        }

        public void UpdateUser(ulong steamid, string playername = "No Name", uint Score = 1) => SteamGameServer.BUpdateUserData(new CSteamID(steamid), playername, Score);

        public bool HandleAuthTicketResponse(ulong SteamID, ulong OwnerID, SteamAuthTicketResponse Response)
        {
            var connection = GameManager.Instance.GetNetworkPlayerBySteamId(new CSteamID(SteamID));
            if (!connection.HasValue) return false;

            var session = GameManager.Instance.GetSession(connection.Value);

            Interface.Core.LogInfo($"[SteamManager]: {SteamID}/{session.CGSPlayer.DisplayName} /{OwnerID} {Response}");
            session.CGSPlayer.SteamResponse = Response;
            session.CGSPlayer.UserData.OwnerID = OwnerID;

            if (GameManager.Instance.IsAdmin(session.SteamId)) return false;

            switch (Response)
            {
                case SteamAuthTicketResponse.VACBanned:
                case SteamAuthTicketResponse.PublisherIssuedBan:
                    session.ValidationResponse = (Response == SteamAuthTicketResponse.PublisherIssuedBan) ? EAuthSessionResponse.k_EAuthSessionResponsePublisherIssuedBan : EAuthSessionResponse.k_EAuthSessionResponseVACBanned;
                    HurtworldLoader.GameManager.KickPlayer(connection.Value, "Banned: Account is banned");
                    return false;

                case SteamAuthTicketResponse.OK:
                    session.ValidationResponse = EAuthSessionResponse.k_EAuthSessionResponseOK;
                    return true;

                default:
                    session.ValidationResponse = (EAuthSessionResponse)((int)Response);
                    HurtworldLoader.GameManager.KickPlayer(connection.Value, $"Kicked: Steam Auth Failed - {Response}");
                    return false;
            }
        }
    }
}
