﻿using CGSGameManager.Core;
using CGSGameManager.Core.Extensions;
using CGSGameManager.Hurtworld.GameServers;

namespace CGSGameManager.Hurtworld
{
    public class HurtworldLoader : Extension
    {
        public override string ExtensionName => "Hurtworld";
        public override string ExtensionAuthor => "KahunaElGrande";
        public override VersionNumber ExtensionVersion => VersionNumber.CGSCurrent();

        public static HurtworldGameManager GameManager { get; private set; }
        public HurtworldCore PlCore { get; private set; }

        public HurtworldLoader(ExtensionManager manager) : base(manager)
        {
        }

        public override void Load()
        {
            GameManager = new HurtworldGameManager();
            LoadPlugin(PlCore = new HurtworldCore());
        }

        public override void OnUnload()
        {
            UnregisterPlugin(PlCore);
        }
    }
}
